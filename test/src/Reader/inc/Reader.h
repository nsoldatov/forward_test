#pragma once

//#include <istream>
#include <fstream>
#include <string>
#include <vector>

class Reader
{
public:
    explicit Reader(const std::string& fname);
    Reader() = delete;
    Reader(const Reader&) = delete;
    Reader(Reader&&) = delete;
    Reader& operator=(const Reader&) = delete;
    Reader& operator=(Reader&&) = delete;

    void read();
//    const std::vector<double>& parameters() const;
    double I() const;
    double T_oh() const;
    double Hm() const;
    double Hw() const;
    double C() const;

    const std::vector<double> moments();
    const std::vector<double> angvels();

private:
    void write_to(std::vector<double>&);

    std::ifstream m_ifs;
    std::vector<double> params;
    std::vector<double> m_moms;
    std::vector<double> m_avels;

};

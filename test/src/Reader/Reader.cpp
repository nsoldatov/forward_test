#include "Reader.h"

#include <iostream>
#include <sstream>

Reader::Reader(const std::string& fname)
    : m_ifs(fname)
{}

double Reader::I() const { return params[0];}
double Reader::T_oh() const { return params[1]; }
double Reader::Hm() const { return params[2]; }
double Reader::Hw() const { return params[3]; }
double Reader::C() const { return params[4]; }

const std::vector<double> Reader::moments() { return m_moms; }
const std::vector<double> Reader::angvels() { return m_avels; }

void Reader::read()
{
    if (!m_ifs.is_open())
    {
        std::cout << "Failed to open file." << std::endl;
        return;
    }

    const int nlines = 7;
    for (int i = 0; i < nlines - 2; ++i)
    {
        write_to(params);

    }
    write_to(m_moms);
    write_to(m_avels);
}

void Reader::write_to(std::vector<double>& vec)
{
    double number;
    std::string line;
    std::getline(m_ifs, line);
    if (line.size() == 0)
    {
        std::cout << "Incorrect input file." << std::endl;
        return;
    }
    std::istringstream iss(line);
    while (iss >> number)
    {
        vec.push_back(number);
    }
}

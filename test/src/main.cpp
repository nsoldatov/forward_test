#include <iostream>

#include "ICEngineCreator.h"
#include "TestIterationStrategy.h"
#include "TestStand.h"
#include "Reader.h"

int main()
{
    /*
     * Input Data
     */
    Reader rd("test_configs.txt");
    rd.read();

    Engine_Creator* ecr = new ICEngine_Creator();

    Engine* eng = ecr->create({rd.I(), rd.T_oh(), rd.Hm(), rd.Hw(), rd.C()});
    Test_Stand stand(std::make_unique<Test_Iteration_Strategy>(eng, rd.moments(), rd.angvels()));
    stand.prepare_parameters();
    stand.test();

    delete ecr;
    return 0;
}


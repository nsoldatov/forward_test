#pragma once

#include "TestStrategy.h"

#include <vector>

using v_Moments = std::vector<double>;
using v_AngVels = std::vector<double>;

class Test_Iteration_Strategy : public Test_Strategy
{
public:
    Test_Iteration_Strategy() = delete;
    Test_Iteration_Strategy(Engine* pengine, const v_Moments&, const v_AngVels&, double dt = 0.005);

	Test_Iteration_Strategy(const Test_Iteration_Strategy&) = default;
	Test_Iteration_Strategy(Test_Iteration_Strategy&&) = default;

	Test_Iteration_Strategy& operator=(const Test_Iteration_Strategy&) = default;
	Test_Iteration_Strategy& operator=(Test_Iteration_Strategy&&) = default;

    std::optional<double> run_test() const override;
    void run_prepare_parameters() override;
protected:
    double m_dt;
	v_Moments m_moments;
	v_AngVels m_angvels;
};

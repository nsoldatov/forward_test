#pragma once

#include "Engine.h"

#include <optional>

class Test_Strategy
{
public:
	Test_Strategy() = delete;
	Test_Strategy(Engine* pengine)
		: m_pengine(pengine) {}

	Test_Strategy(const Test_Strategy&) = default;
	Test_Strategy(Test_Strategy&&) = default;

	Test_Strategy& operator=(const Test_Strategy&) = default;
	Test_Strategy& operator=(Test_Strategy&&) = default;

    virtual ~Test_Strategy() = default;

    virtual std::optional<double> run_test() const = 0;
    virtual void run_prepare_parameters() = 0;

protected:
	Engine* m_pengine;
    double T_ambient = -1000;
};

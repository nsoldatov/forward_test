#pragma once

#include "TestStrategy.h"

#include <memory>

class Test_Stand
{
public:
    explicit Test_Stand(std::unique_ptr<Test_Strategy>&&);
	void set_strategy(std::unique_ptr<Test_Strategy>&&);
    void prepare_parameters();
	void test();
private:
	std::unique_ptr<Test_Strategy> m_pstrategy;
};

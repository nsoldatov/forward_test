#include "TestStand.h"
#include <iostream>


Test_Stand::Test_Stand(std::unique_ptr<Test_Strategy>&& pstrategy)
	: m_pstrategy(std::move(pstrategy))
{}

void Test_Stand::set_strategy(std::unique_ptr<Test_Strategy>&& pstrategy)
{
	m_pstrategy = std::move(pstrategy);
}

void Test_Stand::prepare_parameters()
{
    m_pstrategy->run_prepare_parameters();
}

void Test_Stand::test()
{
    std::optional<double> time_sec = m_pstrategy->run_test();
    if (time_sec.has_value())
    {
        std::cout << "\nDone. Time until the engine overheats is " << time_sec.value() <<" seconds." << std::endl;
    }
    else
    {
        std::cout << "\nDone. T_engine is still less than T_ambient. No time to return.\n Maybe you should check input data?" << std::endl;
    }
}

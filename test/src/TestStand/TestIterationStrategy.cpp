#include "TestIterationStrategy.h"
#include <iostream>
#include <cmath>

Test_Iteration_Strategy::Test_Iteration_Strategy(Engine* pengine, const v_Moments& moments, const v_AngVels& angvels, double dt)
    : Test_Strategy(pengine), m_dt(dt), m_moments(moments), m_angvels(angvels)
{}

void Test_Iteration_Strategy::run_prepare_parameters()
{
	std::cout << "I: " << m_pengine->I() << std::endl;
	std::cout << "T_overheat: " << m_pengine->T_oh() << std::endl;
	std::cout << "H_m: " << m_pengine->Hm() << std::endl;
	std::cout << "H_w: " << m_pengine->Hw() << std::endl;
	std::cout << "C: " << m_pengine->C() << std::endl;

    std::cout << "M: {";
	for (auto moment : m_moments)
	{
		std::cout << moment <<"  ";
	}
	std::cout << "}" << std::endl;
	std::cout << "W: {";
	for (auto angvel : m_angvels)
	{
        std::cout << angvel << "  ";
	}
	std::cout << "}" << std::endl;
    std::cout << "Enter T_ambient: ";

    std::cin >> T_ambient;
}

std::optional<double> Test_Iteration_Strategy::run_test() const
{
    const int f_len = m_moments.size() - 1;

    /*
     * 0 <= iter < f_len - 1;
     * when iter == f_len-1 it means we have reached end of the function and T_engine is still < T_ambient.
     * No time_sec to return.
     */
    int iter = 0;
    // angular acceleration
    double eps;
    // Moment "velocity" M_i+1 = M_i + mu_i * dw_i = M_i + mu_i * eps * dt
    double mu =  (m_moments[iter+1] - m_moments[iter]) / (m_angvels[iter+1] - m_angvels[iter]);
    double M = m_moments[iter];
    double w = m_angvels[iter];

    double T_engine = T_ambient;
    double time_sec = 0.0;
    while (T_engine < m_pengine->T_oh())
    {
        if (fabs(w-m_angvels[iter+1]) < m_dt && fabs(M-m_moments[iter+1]) < m_dt)
        {
            ++iter;
            if (iter == f_len)
            {
                return std::nullopt;
            }
            M = m_moments[iter];
            w = m_angvels[iter];
            mu =  (m_moments[iter+1] - m_moments[iter]) / (m_angvels[iter+1] - m_angvels[iter]);
        }

        T_engine += (M * m_pengine->Hw() + w*w * m_pengine->Hm() + (T_ambient - T_engine) * m_pengine->C()) * m_dt;
        time_sec += m_dt;
        eps = M / m_pengine->I();
        w += eps * m_dt;
        M += mu * eps * m_dt;
    }
    return time_sec;
}


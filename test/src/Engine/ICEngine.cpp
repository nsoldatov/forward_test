#include "ICEngine.h"

ICEngine::ICEngine(const std::initializer_list<double>& l)
{
    m_parameters[0] = *l.begin();
    m_parameters[1] = *(l.begin()+1);
    m_parameters[2] = *(l.begin()+2);
    m_parameters[3] = *(l.begin()+3);
    m_parameters[4] = *(l.begin()+4);
}

double ICEngine::I() const
{ 
	return m_parameters[0]; //m_inertia; 
}
double ICEngine::T_oh() const 
{ 
	return m_parameters[1]; //m_temp_overheat; 
}
double ICEngine::Hm() const 
{ 
	return m_parameters[2]; //m_coeff_Hm; 
}
double ICEngine::Hw() const 
{ 
	return m_parameters[3]; //m_coeff_Hw; 
}
double ICEngine::C() const 
{ 
	return m_parameters[4]; //m_coeff_C; 
}


#pragma once

#include "Engine.h"

#include <array>

class ICEngine : public Engine
{
public:
	~ICEngine() = default;
    ICEngine(const std::initializer_list<double>& l);

	double I() const override; //{ return m_inertia; }
	double T_oh() const override; //{ return m_temp_superheat; }
	double Hm() const override; //{ return m_coeff_Hm; }
	double Hw() const override; //{ return m_coeff_Hw; }
	double C() const override; //{ return m_coeff_C; }

//	void set_parameters(const ICEngine_Params& params);

protected:
    ICEngine_Params m_parameters {-1, -1, -1, -1, -1};
};

#pragma once

#include <vector>
#include <memory>
#include <initializer_list>

using ICEngine_Params = std::array<double, 5>;

class Engine
{
public:
	virtual ~Engine() = default;

	virtual double I() const = 0; //{ return m_inertia; }
	virtual double T_oh() const = 0; //{ return m_temp_superheat; }
	virtual double Hm() const = 0; //{ return m_coeff_Hm; }
	virtual double Hw() const = 0; //{ return m_coeff_Hw; }
	virtual double C() const = 0; //{ return m_coeff_C; }
};

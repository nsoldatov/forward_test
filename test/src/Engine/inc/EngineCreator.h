#pragma once

#include "Engine.h"

class Engine_Creator
{
public:    
    virtual ~Engine_Creator() = default;
    virtual Engine* create(const std::initializer_list<double>& l) const = 0;
};

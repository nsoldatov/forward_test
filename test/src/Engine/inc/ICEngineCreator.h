#pragma once

#include "EngineCreator.h"
#include "Engine.h"

class ICEngine_Creator : public Engine_Creator
{
public:
//    ICEngine_Creator(const ICEngine_Params&);

    ~ICEngine_Creator() = default;
    Engine* create(const std::initializer_list<double>& l) const override;
protected:
//     ICEngine_Params m_params;
};
